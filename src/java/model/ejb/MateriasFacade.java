/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.entity.Materias;

/**
 *
 * @author p02sotos
 */
@Stateless
public class MateriasFacade extends AbstractFacade<Materias> {
    @PersistenceContext(unitName = "BiblioPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public MateriasFacade() {
        super(Materias.class);
    }
    
    public Materias findMateriaByNombre(String nombre) {
         Materias resultado;
        Query q = em.createNamedQuery("findMateriasByNombre");
        q.setParameter("nombre", nombre);
         try {
              resultado = (Materias)q.getSingleResult();
             
         } catch (NoResultException ex) {
             return null;
         }
          return resultado;

        
    }
    
}
