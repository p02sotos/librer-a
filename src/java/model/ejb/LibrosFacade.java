/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.ejb;

import java.util.List;
import java.util.StringTokenizer;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.entity.Libros;

/**
 *
 * @author p02sotos
 */
@Stateless
public class LibrosFacade extends AbstractFacade<Libros> {

    @PersistenceContext(unitName = "BiblioPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public LibrosFacade() {
        super(Libros.class);
    }

    public List<Libros> findLibrosByAutor(String nombre) {
        List<Libros> resultado;
        Query q = em.createNamedQuery("findLibrosByAutor");
        q.setParameter("autor", nombre);
        resultado = (List<Libros>)q.getResultList();
        return resultado;
    }
     public List<Libros> findLibrosByMateria(String nombre) {
        List<Libros> resultado;
        Query q = em.createNamedQuery("findLibrosByMateria");
        q.setParameter("materia", nombre);
        resultado = (List<Libros>)q.getResultList();
        return resultado;
    }
     public List<Libros> findLibrosByIsbn(String nombre) {
        List<Libros> resultado;
        Query q = em.createNamedQuery("findLibrosByIsbn");
        q.setParameter("isbn", nombre);
        resultado = (List<Libros>)q.getResultList();
        return resultado;
    }
     public List<Libros> findLibrosByTitulo(String nombre) {
        List<Libros> resultado;
        Query q = em.createNamedQuery("findLibrosByTitulo");
        q.setParameter("titulo", nombre);
        resultado = (List<Libros>)q.getResultList();
        return resultado;
    }
     public List<Libros> findLibrosByEstanteriaSignatura(String estanteria) {
        List<Libros> resultado;
        Query q = em.createNamedQuery("findLibrosByEstanteriaSignatura");
        q.setParameter("signatura", estanteria);
        resultado = (List<Libros>)q.getResultList();
        return resultado;
    }
     public List<Libros> findLibrosAvanced(String query) {
        List<Libros> resultado;
        Query q = em.createQuery(query);        
        resultado = (List<Libros>)q.getResultList();
        return resultado;
    }
     
    
}
