/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.ejb;

import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import model.entity.Autores;

/**
 *
 * @author p02sotos
 */
@Stateless
public class AutoresFacade extends AbstractFacade<Autores> {
    @PersistenceContext(unitName = "BiblioPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public AutoresFacade() {
        super(Autores.class);
    }

    @Override
    public List<Autores> findAll() {
        
        Query q = em.createNamedQuery("findAllAutoresOrderByNormalizado");
        List<Autores> resultado = (List<Autores>)q.getResultList();
        return resultado;
    }
    
     public Autores findAutorByNormalizado(String nombre) {
         Autores resultado;
        Query q = em.createNamedQuery("findAutoresByNormalizado");
        q.setParameter("normalizado", nombre);
         try {
              resultado = (Autores)q.getSingleResult();
             
         } catch (NoResultException ex) {
             return null;
         }
          return resultado;

        
    }
    
    
    
    
    
}
