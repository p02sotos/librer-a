/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.ejb;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.entity.Estanterias;

/**
 *
 * @author p02sotos
 */
@Stateless
public class EstanteriasFacade extends AbstractFacade<Estanterias> {
    @PersistenceContext(unitName = "BiblioPU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EstanteriasFacade() {
        super(Estanterias.class);
    }
    
}
