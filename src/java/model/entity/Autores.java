/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author p02sotos
 */
@NamedQueries(
        {
    @NamedQuery(name = "findAllAutores", query = "SELECT a FROM Autores a"),
    @NamedQuery(name = "findAllAutoresOrderByNormalizado", query = "SELECT a FROM Autores a ORDER BY a.normalizado"),
    @NamedQuery(name = "findAutoresById", query = "SELECT a FROM Autores a WHERE a.id = :id"),
    @NamedQuery(name = "findAutoresByNormalizado", query = "SELECT a FROM Autores a WHERE a.normalizado = :normalizado"),
    @NamedQuery(name = "findAutoresByVariantes", query = "SELECT a FROM Autores a WHERE a.variantes = :variantes"),
    @NamedQuery(name = "findAutoresByUsadoPor", query = "SELECT a FROM Autores a WHERE a.usadoPor = :usadoPor"),
    @NamedQuery(name = "findAutoresByFechaCreacion", query = "SELECT a FROM Autores a WHERE a.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "findAutoresByFechaModificacion", query = "SELECT a FROM Autores a WHERE a.fechaModificacion = :fechaModificacion")
})
@Entity
public class Autores implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String normalizado;
    private String variantes;
    private String usadoPor;
    @Temporal(TemporalType.DATE)
    private Date fechaCreacion;
    @Temporal(TemporalType.DATE)
    private Date fechaModificacion;
    

    public Autores(String normalizado, String variantes, String usadoPor, Date fechaCreacion, Date fechaModificacion) {

        this.normalizado = normalizado;
        this.variantes = variantes;
        this.usadoPor = usadoPor;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion;
    }
    public Autores(String normalizado, String variantes, String usadoPor) {

        this.normalizado = normalizado;
        this.variantes = variantes;
        this.usadoPor = usadoPor;
        
    }

    public Autores(String normalizado) {
        this.normalizado = normalizado;
    }

    public Autores() {
    }

    public String getNormalizado() {
        return normalizado;
    }

    public void setNormalizado(String normalizado) {
        this.normalizado = normalizado;
    }

    public String getVariantes() {
        return variantes;
    }

    public void setVariantes(String variantes) {
        this.variantes = variantes;
    }

    public String getUsadoPor() {
        return usadoPor;
    }

    public void setUsadoPor(String usadoPor) {
        this.usadoPor = usadoPor;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Autores)) {
            return false;
        }
        Autores other = (Autores) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.entity.Autores[ id=" + id + " ]";
    }
}
