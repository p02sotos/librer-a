/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author p02sotos
 */
@NamedQueries(
        {
    @NamedQuery(name = "findAllMaterias", query = "SELECT m FROM Materias m"),
    @NamedQuery(name = "findMateriasById", query = "SELECT m FROM Materias m WHERE m.id = :id"),
    @NamedQuery(name = "findMateriasByNombre", query = "SELECT m FROM Materias m WHERE m.nombre = :nombre"),
    @NamedQuery(name = "findMateriasByDescripcion", query = "SELECT m FROM Materias m WHERE m.descripcion = :descripcion"),
    @NamedQuery(name = "findMateriasByVease", query = "SELECT m FROM Materias m WHERE m.vease = :vease"),
    @NamedQuery(name = "findMateriasByUsadoPor", query = "SELECT m FROM Materias m WHERE m.usadoPor = :usadoPor"),
    @NamedQuery(name = "findMateriasByFechaCreacion", query = "SELECT m FROM Materias m WHERE m.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "findMateriasByFechaModificacion", query = "SELECT m FROM Materias m WHERE m.fechaModificacion = :fechaModificacion")
})
@Entity
public class Materias implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO )
    private Long id;
    @Column (nullable = false)
    private String nombre;
    private String descripcion;
    private String vease;
    private String usadoPor;
    @Temporal(TemporalType.DATE)
    private Date fechaCreacion;
    @Temporal(TemporalType.DATE)
    private Date fechaModificacion;

    public Materias() {
    }

    public Materias(String nombre, String descripcion, String vease, String usadoPor) {
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.vease = vease;
        this.usadoPor = usadoPor;

    }
    public Materias(String nombre) {
        this.nombre = nombre;
        

    }

    public Materias(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getVease() {
        return vease;
    }

    public void setVease(String vease) {
        this.vease = vease;
    }

    public String getUsadoPor() {
        return usadoPor;
    }

    public void setUsadoPor(String usadoPor) {
        this.usadoPor = usadoPor;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Materias)) {
            return false;
        }
        Materias other = (Materias) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.entity.Materias[ id=" + id + " ]";
    }
}
