package model.entity;

import java.io.Serializable;
import java.net.URL;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


@NamedQueries({
    @NamedQuery(name = "findAllEstanterias", query = "SELECT e FROM Estanterias e"),
    @NamedQuery(name = "findEstanteriasById", query = "SELECT e FROM Estanterias e WHERE e.id = :id"),
    @NamedQuery(name = "findEstanteriasBySignatura", query = "SELECT e FROM Estanterias e WHERE e.signatura = :signatura"),
    @NamedQuery(name = "findEstanteriasByDescripcion", query = "SELECT e FROM Estanterias e WHERE e.descripcion = :descripcion")
})
@Entity
public class Estanterias implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String signatura;
    private String descripcion;
    private URL imagen;
    
    @Temporal(javax.persistence.TemporalType.DATE)
    Date FechaC;
    @Temporal(javax.persistence.TemporalType.DATE)
    Date FechaM;
    

    public Estanterias() {
    }

    public Estanterias(String signatura, String descripcion, URL imagen) {
        this.signatura = signatura;
        this.descripcion = descripcion;
        this.imagen = imagen;
    }

    public Estanterias(String signatura, String descripcion) {
        this.signatura = signatura;
        this.descripcion = descripcion;
    }
    
    
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getSignatura() {
        return signatura;
    }

    public void setSignatura(String signatura) {
        this.signatura = signatura;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public URL getImagen() {
        return imagen;
    }

    public void setImagen(URL imagen) {
        this.imagen = imagen;
    }

    public Date getFechaC() {
        return FechaC;
    }

    public void setFechaC(Date FechaC) {
        this.FechaC = FechaC;
    }

    public Date getFechaM() {
        return FechaM;
    }

    public void setFechaM(Date FechaM) {
        this.FechaM = FechaM;
    }
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Estanterias)) {
            return false;
        }
        Estanterias other = (Estanterias) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.entity.Estanterias[ id=" + id + " ]";
    }
}
