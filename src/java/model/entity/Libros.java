/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package model.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author p02sotos
 */
@NamedQueries(
        {
    @NamedQuery(name = "findAllLibros", query = "SELECT l FROM Libros l"),
    @NamedQuery(name = "findLibrosById", query = "SELECT l FROM Libros l WHERE l.id = :id"),
    @NamedQuery(name = "findLibrosByMateria", query = "SELECT l FROM Libros l JOIN  l.materias m WHERE m.nombre LIKE :materia"),
    @NamedQuery(name = "findLibrosByAutor", query = "SELECT l FROM Libros l JOIN  l.autores a WHERE a.normalizado LIKE :autor"),
    @NamedQuery(name = "findLibrosByEstanteriaSignatura", query = "SELECT l FROM Libros l JOIN l.estanteria e  WHERE e.signatura LIKE :signatura"),
    @NamedQuery(name = "findLibrosByTitulo", query = "SELECT l FROM Libros l WHERE l.titulo LIKE :titulo"),
    @NamedQuery(name = "findLibrosByIsbn", query = "SELECT l FROM Libros l WHERE l.isbn LIKE :isbn"),
    @NamedQuery(name = "findLibrosByIssn", query = "SELECT l FROM Libros l WHERE l.issn = :issn"),
    @NamedQuery(name = "findLibrosByLugar", query = "SELECT l FROM Libros l WHERE l.lugar = :lugar"),
    @NamedQuery(name = "findLibrosByEditorial", query = "SELECT l FROM Libros l WHERE l.editorial = :editorial"),
    @NamedQuery(name = "findLibrosByAnno", query = "SELECT l FROM Libros l WHERE l.anno = :anno"),
    @NamedQuery(name = "findLibrosByFechaCreacion", query = "SELECT l FROM Libros l WHERE l.fechaCreacion = :fechaCreacion"),
    @NamedQuery(name = "findLibrosByFechaModificacion", query = "SELECT l FROM Libros l WHERE l.fechaModificacion = :fechaModificacion")
})
@Entity
public class Libros implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String titulo;
    private String isbn;
    private String issn;
    private String lugar;
    private String editorial;
    private String anno;
    @Temporal(TemporalType.DATE)
    private Date fechaCreacion;
    @Temporal(TemporalType.DATE)
    private Date fechaModificacion;
    @OneToMany
    private List<Autores> autores = new ArrayList<>();
    @OneToMany
    private List<Materias> materias = new ArrayList<>();
    @ManyToOne
    private Estanterias estanteria;

    public Libros() {
    }

    public Libros(String titulo, String isbn, String issn, String lugar, String editorial, String anno) {



        this.titulo = titulo;
        this.isbn = isbn;
        this.issn = issn;
        this.lugar = lugar;
        this.editorial = editorial;
        this.anno = anno;

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getIssn() {
        return issn;
    }

    public void setIssn(String issn) {
        this.issn = issn;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getEditorial() {
        return editorial;
    }

    public void setEditorial(String editorial) {
        this.editorial = editorial;
    }

    public String getAnno() {
        return anno;
    }

    public void setAnno(String anno) {
        this.anno = anno;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Date getFechaModificacion() {
        return fechaModificacion;
    }

    public void setFechaModificacion(Date fechaModificacion) {
        this.fechaModificacion = fechaModificacion;
    }

    public List<Autores> getAutores() {
        return autores;
    }

    public void setAutores(List<Autores> autores) {
        this.autores = autores;
    }

    public List<Materias> getMaterias() {
        return materias;
    }

    public void setMaterias(List<Materias> materias) {
        this.materias = materias;
    }

    public Estanterias getEstanteria() {
        return estanteria;
    }

    public void setEstanteria(Estanterias estanteria) {
        this.estanteria = estanteria;
    }
    
    

    public void addAutor(Autores autor) {


        if (!getAutores().contains(autor)) {
            getAutores().add(autor);
        }
    }

    public void addMateria(Materias materia) {


        if (!getMaterias().contains(materia)) {
            getMaterias().add(materia);
        }
    }

    public void clearAutores() {
        this.autores.clear();
    }

    public void clearMaterias() {
        this.materias.clear();
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Libros)) {
            return false;
        }
        Libros other = (Libros) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.entity.Libros[ id=" + id + " ]";
    }

    public String getMateriasString() {
        StringBuffer string = new StringBuffer();
        List<Materias> mat = getMaterias();
        for (Materias materias1 : mat) {
            string.append(materias1.getNombre());
            string.append(",");
        }
        if (string.length() == 0) {
            return "";
        } else {
            string.deleteCharAt(string.length() - 1);
            return string.toString();
        }
    }
}
