/*
 Falta una parte para comprobar que la materia no está repetida.
 */
package control.servlets;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.ejb.MateriasFacade;
import model.entity.Materias;
import model.entity.Materias;

/**
 *
 * @author p02sotos
 */
public class MateriasServlet extends HttpServlet {

    @EJB
    private MateriasFacade materiasFacade;
    private HttpSession sesion;
    private List<Materias> materias;
    private Materias materia;
    int sel;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        String nombre;
        String descripcion;
        String vease;
        String usadoPor;
        Date fechaCreacion;
        Date fechaModificacion;


        sesion = request.getSession();

        String accion = request.getParameter("accion");
        String selecion = request.getParameter("seleccion");
        if (selecion != null) {
            try {
                sel = Integer.parseInt(selecion);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        if (accion.equalsIgnoreCase("materiaNuevo")) {
            response.sendRedirect("/Biblio/materias/materiasNuevo.jsp");

        }
        if (accion.equalsIgnoreCase("materiaVer")) {
            setRequestMateriaSelect(request);
            request.getRequestDispatcher("materias/materiasVer.jsp").forward(request, response);
        }
        if (accion.equalsIgnoreCase("materiaEditar")) {
            setRequestMateriaSelect(request);
            request.getRequestDispatcher("materias/materiasEditar.jsp").forward(request, response);
        }
        /*
         * Opciones de materiasNuevo.jsp
         */
        if (accion.equalsIgnoreCase("guardarMateria")) {
            nombre = request.getParameter("nombre");
            descripcion = request.getParameter("descripcion");
            vease = request.getParameter("vease");
            usadoPor = request.getParameter("usadoPor");
            Date fechaC = new Date();
            materia = new Materias(nombre, descripcion, vease, usadoPor);
            materia.setFechaCreacion(fechaC);
            materia.setFechaModificacion(fechaC);
            materiasFacade.create(materia);
            sesion.removeAttribute("materias");
            materias = materiasFacade.findAll();
            sesion.setAttribute("materias", materias);
            response.sendRedirect("/Biblio/materias/materiasLista.jsp");
        }
        if (accion.equalsIgnoreCase("volverMateriaNuevo")) {
            response.sendRedirect("/Biblio/materias/materiasLista.jsp");
        }

        /*
         * Opciones de materiasEditar.jsp
         */

        if (accion.equalsIgnoreCase("materiaModificar")) {
            nombre = request.getParameter("nombre");
            descripcion = request.getParameter("descripcion");
            vease = request.getParameter("vease");
            usadoPor = request.getParameter("usadoPor");
            Date fechaM = new Date();
            materia.setNombre(nombre);
            materia.setDescripcion(descripcion);
            materia.setVease(vease);
            materia.setUsadoPor(usadoPor);
            materia.setFechaModificacion(fechaM);
            materiasFacade.edit(materia);
            sesion.removeAttribute("materias");
            materias = materiasFacade.findAll();
            sesion.setAttribute("materias", materias);
            response.sendRedirect("/Biblio/materias/materiasLista.jsp");
        }
        if (accion.equalsIgnoreCase("volverMateriaEditar")) {
            response.sendRedirect("/Biblio/materias/materiasLista.jsp");
        }
        /*
         * Opciones de materiasVer.jsp
         */
        if (accion.equalsIgnoreCase("volverMateriaVer")) {
            response.sendRedirect("/Biblio/materias/materiasLista.jsp");
        }
        /*
         * Opciones de materiasLista.jsp
         */

        if (accion.equalsIgnoreCase("Borrar")) {
            request.setAttribute("confirmar", "true");
            sesion.setAttribute("seleccion", sel);
            request.getRequestDispatcher("/materias/materiasLista.jsp").forward(request, response);
        }

        if (accion.equalsIgnoreCase("Confirmar")) {
            sel = (int) sesion.getAttribute("seleccion");
            Materias mat = materias.get(sel - 1);
            materiasFacade.remove(mat);
            materias = materiasFacade.findAll();
            sesion.setAttribute("materias", materias);
            sesion.removeAttribute("seleccion");
            response.sendRedirect("/Biblio/materias/materiasLista.jsp");
        }
        if (accion.equalsIgnoreCase("materiaVer")) {
            Materias materia = materias.get(sel - 1);
            sesion.setAttribute("materia", materia);
            request.getRequestDispatcher("/materias/materiasVer.jsp").forward(request, response);
        }








    }

    private void setRequestMateriaSelect(HttpServletRequest request) {
        materias = (List<Materias>) sesion.getAttribute("materias");
        materia = materias.get(sel - 1);
        request.setAttribute("materia", materia);

    }

    private void setSesionMateriaSelect(HttpServletRequest request) {
        materias = (List<Materias>) sesion.getAttribute("materias");
        materia = materias.get(sel - 1);
        sesion.setAttribute("materia", materia);

    }

    private void clearMaterias() {
        sesion.removeAttribute("materias");
        materias.clear();
    }

    private void clearMateria() {
        sesion.removeAttribute("materia");
        materia = new Materias();
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
