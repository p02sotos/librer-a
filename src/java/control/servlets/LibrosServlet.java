/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package control.servlets;

import control.SesionControl;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.StringTokenizer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.ejb.AutoresFacade;
import model.ejb.EstanteriasFacade;
import model.ejb.LibrosFacade;
import model.ejb.MateriasFacade;
import model.entity.Autores;
import model.entity.Estanterias;
import model.entity.Libros;
import model.entity.Materias;

/**
 *
 * @author p02sotos
 */
public class LibrosServlet extends HttpServlet {
    @EJB
    private EstanteriasFacade estanteriasFacade;

    SesionControl sesionControl = lookupSesionControlBean();
    @EJB
    private MateriasFacade materiasFacade;
    @EJB
    private LibrosFacade librosFacade;
    @EJB
    private AutoresFacade autoresFacade;
    
    private HttpSession sesion;
    private List<Autores> autores;
    private List<Libros> libros;
    private List<Materias> materias;
    private List<Estanterias> estanterias;
    private Autores autor;
    private Materias materia;
    private Libros libro;
    int sel;
    List contador = new ArrayList(1);
    String titulo;
    String isbn;
    String issn;
    String lugar;
    String editorial;
    String anno;
    Estanterias estanteria;
    String[] autoresGuardar;
    String materiasString;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {




        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        sesion = request.getSession();
        estanterias = (List<Estanterias>)estanteriasFacade.findAll();
        sesion.setAttribute("estanterias", estanterias);

        String accion = request.getParameter("accion");
        String selecion = request.getParameter("seleccion");
        if (selecion != null) {
            try {
                sel = Integer.parseInt(selecion);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        /*
         * Acciones para librosLista.jsp
         */

        if (accion.equalsIgnoreCase("libroNuevo")) {
            sesion.removeAttribute("materiasString");
            sesion.removeAttribute("libro");
            sesion.removeAttribute("autores");
            sesion.removeAttribute("estanteria");
            libro = new Libros();
            autores = libro.getAutores();
            autores.add(new Autores(""));
            sesion.setAttribute("autores", autores);
            sesion.setAttribute("libro", libro);
            response.sendRedirect("/Biblio/libros/librosNuevo.jsp");

        }
        if (accion.equalsIgnoreCase("libroEditar")) {
            libro = new Libros();
            setSesionLibroSelect(request);
            autores = libro.getAutores();
            materiasString = libro.getMateriasString();
            sesion.setAttribute("materiasString", materiasString);
            sesion.setAttribute("autores", autores);
            request.getRequestDispatcher("libros/librosEditar.jsp").forward(request, response);

        }

        if (accion.equalsIgnoreCase("Borrar")) {
            request.setAttribute("confirmar", "true");
            sesion.setAttribute("seleccion", sel);
            if (request.getParameter("borrarDesdeBusqueda") != null) {
                request.setAttribute("busquedaRealizada", "true");
                request.getRequestDispatcher("/index.jsp").forward(request, response);
            } else {
                request.getRequestDispatcher("/libros/librosLista.jsp").forward(request, response);
            }
        }

        if (accion.equalsIgnoreCase("Confirmar")) {
            sel = (int) sesion.getAttribute("seleccion");
            if (request.getParameter("confirmarDesdeBusqueda") != null) {
                libros = (List<Libros>) sesion.getAttribute("resultadoBusqueda");
            }
            Libros libro = libros.get(sel - 1);
            librosFacade.remove(libro);
            libros.remove(sel - 1);
            //libros = librosFacade.findAll();
            sesion.setAttribute("libros", libros);
            sesion.removeAttribute("seleccion");
            if (request.getParameter("confirmarDesdeBusqueda") != null) {
                response.sendRedirect("/Biblio/index.jsp");
            } else {
                response.sendRedirect("/Biblio/libros/librosLista.jsp");
            }
        }

        /*
         * Opciones de librosNuevo.jsp
         */
        if (accion.equalsIgnoreCase("guardarLibro")) {


            getParameters(request);
            Date fechaC = new Date();
            libro = new Libros(titulo, isbn, issn, lugar, editorial, anno);
            libro.setFechaCreacion(fechaC);
            libro.setFechaModificacion(fechaC);
            libro.setEstanteria(estanteria);

            //AÑADIR AUTORES

            addAutores(autoresGuardar);
            //AÑADIR MATERIAS
            addMaterias(materiasString);



            librosFacade.create(libro);
            sesion.setAttribute("materiasString", materiasString);
            sesion.setAttribute("libro", libro);
            sesion.removeAttribute("libros");
            libros = librosFacade.findAll();
            sesion.setAttribute("libros", libros);
            response.sendRedirect("/Biblio/libros/librosLista.jsp");



        }
        if (accion.equalsIgnoreCase("volverLibroNuevo")) {
            response.sendRedirect("/Biblio/libros/librosLista.jsp");
        }

        if (accion.equalsIgnoreCase("agregarAutor")) {
            autores.clear();
            setParameters(request);
            autores = libro.getAutores();
            sesion.setAttribute("materiasString", materiasString);
            autores.add(new Autores(""));
            sesion.setAttribute("autores", autores);



            request.getRequestDispatcher("/libros/librosNuevo.jsp").forward(request, response);

        }

        /*
         * Opciones de librossEditar.jsp
         */
        if (accion.equalsIgnoreCase("libroModificar")) {
            libro.clearAutores();
            libro.clearMaterias();
            setParameters(request);
            librosFacade.edit(libro);
            sesion.removeAttribute("libros");
            libros = librosFacade.findAll();
            sesion.setAttribute("libros", libros);
            response.sendRedirect("/Biblio/libros/librosLista.jsp");
        }
        if (accion.equalsIgnoreCase("agregarAutorModificar")) {

            getParameters(request);
            sesion.setAttribute("materiasString", materiasString);


            autores.add(new Autores(""));
            sesion.setAttribute("autores", autores);

            request.getRequestDispatcher("/libros/librosEditar.jsp").forward(request, response);

        }
        if (accion.equalsIgnoreCase("volverLibroEditar")) {
            response.sendRedirect("/Biblio/libros/librosLista.jsp");
        }
        /*
         * Opciones de librosVer.jsp
         */
        if (accion.equalsIgnoreCase("libroVer")) {
            response.sendRedirect("/Biblio/libros/librosVer.jsp");
        }
        if (accion.equalsIgnoreCase("volverLibrosVer")) {
            response.sendRedirect("/Biblio/libros/librosLista.jsp");
        }
        /* ==================================================*/
        if (accion.equalsIgnoreCase("seleccionarEstanteriaEditar")) {
            setParameters(request);
            sesion.setAttribute("selEstanteria", "Editar");
            request.getRequestDispatcher("/estanterias/estanteriasSeleccionar.jsp").forward(request, response);
        }
        if (accion.equalsIgnoreCase("seleccionarEstanteriaNueva")) {
            setParameters(request);
            sesion.setAttribute("selEstanteria", "Nueva");
            request.getRequestDispatcher("/estanterias/estanteriasSeleccionar.jsp").forward(request, response);
        }




    }

    private void setRequestLibroSelect(HttpServletRequest request) {
        libros = (List<Libros>) sesion.getAttribute("libros");
        libro = libros.get(sel - 1);
        request.setAttribute("libro", libro);

    }

    private void setSesionLibroSelect(HttpServletRequest request) {
        libros = (List<Libros>) sesion.getAttribute("libros");
        libro = libros.get(sel - 1);
        sesion.setAttribute("libro", libro);

    }

    private void getParameters(HttpServletRequest request) {
        titulo = request.getParameter("titulo");
        autoresGuardar = request.getParameterValues("autoresGuardar");
        isbn = request.getParameter("isbn");
        issn = request.getParameter("issn");
        anno = request.getParameter("anno");
        lugar = request.getParameter("lugar");
        editorial = request.getParameter("editorial");
        estanteria = (Estanterias) sesion.getAttribute("estanteria");
        materiasString = request.getParameter("materias");

        //libro = new Libros(titulo, isbn, issn, lugar, editorial, anno);
    }

    private void setParameters(HttpServletRequest request) {
        getParameters(request);
        libro.setTitulo(titulo);
        libro.setIsbn(isbn);
        libro.setIssn(issn);
        libro.setLugar(lugar);
        libro.setEditorial(editorial);
        libro.setAnno(anno);
        libro.setEstanteria(estanteria);
        addAutores(autoresGuardar);
        addMaterias(materiasString);

    }

    /**
     *
     * @param nombreAutor
     * @return true si existe el autor, false si no existe el autor en la base
     * de datos Busca por el nombre Normalizado
     */
    public boolean comprobarAutor(String nombreAutor) {
        if (autoresFacade.findAutorByNormalizado(nombreAutor) == null) {
            return false;
        } else {
            return true;
        }
    }

    public boolean comprobarMateria(String nombreMateria) {
        if (materiasFacade.findMateriaByNombre(nombreMateria) == null) {
            return false;
        } else {
            return true;
        }
    }

    public void addMaterias(String materias) {

        for (StringTokenizer stringTokenizer = new StringTokenizer(materias, ","); stringTokenizer.hasMoreTokens();) {
            String token = stringTokenizer.nextToken();
            String t = token.trim();
            if (!t.equals("")) {
                if (!comprobarMateria(t)) {
                    materia = new Materias(t);
                    materiasFacade.create(materia);
                } else {
                    materia = (Materias) materiasFacade.findMateriaByNombre(t);
                }
                if (!materia.getNombre().equals("")) {
                    libro.addMateria(materia);
                }
            }
        }

    }

    public void addAutores(String[] autores) {
        for (String string : autores) {

            if (!string.equals("")) {
                if (!comprobarAutor(string)) {
                    autor = new Autores(string);
                    autoresFacade.create(autor);
                } else {
                    autor = (Autores) autoresFacade.findAutorByNormalizado(string);
                }
                if (!autor.getNormalizado().equals("")) {
                    libro.addAutor(autor);
                }
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private SesionControl lookupSesionControlBean() {
        try {
            Context c = new InitialContext();
            return (SesionControl) c.lookup("java:global/Biblio/SesionControl!control.SesionControl");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
