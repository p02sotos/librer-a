/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package control.servlets;

import control.SesionControl;
import control.Utils;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.ejb.AutoresFacade;
import model.entity.Autores;

/**
 *
 * @author p02sotos
 */
public class AutoresServlet extends HttpServlet {

    SesionControl sesionControl = lookupSesionControlBean();
    @EJB
    private AutoresFacade autoresFacade;
    private HttpSession sesion;
    private List<Autores> autores;
    private Autores autor;
    int sel;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String normalizado;
        String usadoPor;
        String variantes;





        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        sesion = request.getSession();

        String accion = request.getParameter("accion");
        String selecion = request.getParameter("seleccion");
        if (selecion != null) {

            try {
                sel = Integer.parseInt(selecion);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        /*
         * Opciones de autoresLista.jsp 
         */
        if (accion.equalsIgnoreCase("autorNuevo")) {
            response.sendRedirect("/Biblio/autores/autoresNuevo.jsp");

        }
        if (accion.equalsIgnoreCase("autorVer")) {
            setRequestAutorSelect(request);
            request.getRequestDispatcher("autores/autoresVer.jsp").forward(request, response);
        }
        if (accion.equalsIgnoreCase("autorEditar")) {
            setRequestAutorSelect(request);
            request.getRequestDispatcher("autores/autoresEditar.jsp").forward(request, response);
        }
        /*
         * Opciones de autoresNuevo.jsp
         */
        if (accion.equalsIgnoreCase("guardarAutor")) {
            normalizado = request.getParameter("normalizado");
            variantes = request.getParameter("vease");
            usadoPor = request.getParameter("usadoPor");
            Date fechaC = new Date();
            autor = new Autores(normalizado, variantes, usadoPor);
            autor.setFechaCreacion(fechaC);
            autor.setFechaModificacion(fechaC);
            autoresFacade.create(autor);
            sesion.removeAttribute("autores");
            autores = autoresFacade.findAll();
            sesion.setAttribute("autores", autores);
            response.sendRedirect("/Biblio/autores/autoresLista.jsp");
        }
        if (accion.equalsIgnoreCase("volverAutorNuevo")) {
            response.sendRedirect("/Biblio/autores/autoresLista.jsp");
        }

        /*
         * Opciones de autoresEditar.jsp
         */

        if (accion.equalsIgnoreCase("autorModificar")) {
            normalizado = request.getParameter("normalizado");
            variantes = request.getParameter("variantes");
            usadoPor = request.getParameter("usadoPor");
            Date fechaM = new Date();
            autor.setNormalizado(normalizado);
            autor.setVariantes(variantes);
            autor.setUsadoPor(usadoPor);
            autor.setFechaModificacion(fechaM);
            autoresFacade.edit(autor);
            sesion.removeAttribute("autores");
            autores = autoresFacade.findAll();
            sesion.setAttribute("autores", autores);
            response.sendRedirect("/Biblio/autores/autoresLista.jsp");
        }
        if (accion.equalsIgnoreCase("volverAutorEditar")) {
            response.sendRedirect("/Biblio/autores/autoresLista.jsp");
        }
        /*
         * Opciones de autoresVer.jsp
         */
        if (accion.equalsIgnoreCase("volverAutorVer")) {
            response.sendRedirect("/Biblio/autores/autoresLista.jsp");
        }





    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private SesionControl lookupSesionControlBean() {
        try {
            Context c = new InitialContext();
            return (SesionControl) c.lookup("java:global/Biblio/SesionControl!control.SesionControl");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    private void setRequestAutorSelect(HttpServletRequest request) {
        autores = (List<Autores>) sesion.getAttribute("autores");
        autor = autores.get(sel - 1);
        request.setAttribute("autor", autor);

    }

    private void setSesionAutorSelect(HttpServletRequest request) {
        autores = (List<Autores>) sesion.getAttribute("autores");
        autor = autores.get(sel - 1);
        sesion.setAttribute("autor", autor);

    }

    private void clearAutores() {
        sesion.removeAttribute("autores");
        autores.clear();
    }

    private void clearAutor() {
        sesion.removeAttribute("autor");
        autor = new Autores();
    }
}
