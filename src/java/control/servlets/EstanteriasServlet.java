/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package control.servlets;

import java.io.IOException;
import java.net.URL;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.ejb.AutoresFacade;
import model.ejb.EstanteriasFacade;
import model.entity.Estanterias;
import model.entity.Materias;

/**
 *
 * @author p02sotos
 */
public class EstanteriasServlet extends HttpServlet {

    @EJB
    private EstanteriasFacade estanteriasFacade;
    private AutoresFacade autoresFacade;
    private HttpSession sesion;
    private List<Estanterias> estanterias;
    private Estanterias estanteria;
    int sel;
    private String signatura;
    private String descripcion;
    private String imagen;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        sesion = request.getSession();

        String accion = request.getParameter("accion");
        String selecion = request.getParameter("seleccion");
        if (selecion != null) {

            try {
                sel = Integer.parseInt(selecion);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }
        /*
         * Opciones de estanteriasLista.jsp 
         */
        if (accion.equalsIgnoreCase("estanteriaNueva")) {
            response.sendRedirect("/Biblio/estanterias/estanteriasNueva.jsp");

        }
        if (accion.equalsIgnoreCase("estanteriasEditar")) {
            setRequestEstanteriarSelect(request);
            request.getRequestDispatcher("estanterias/estanteriasEditar.jsp").forward(request, response);
        }

        /*
         * Opciones de estanteriasNueva.jsp
         */
        if (accion.equalsIgnoreCase("guardarEstanteria")) {
            signatura = request.getParameter("signatura");
            descripcion = request.getParameter("descripcion");
            imagen = request.getParameter("imagen");
            estanteria = new Estanterias(signatura, descripcion);
            if (!imagen.equals("")) {
                URL urlImagen = new URL(imagen);
                estanteria.setImagen(urlImagen);
            }
            Date fechaC = new Date();

            estanteria.setFechaC(fechaC);
            estanteria.setFechaM(fechaC);
            estanteriasFacade.create(estanteria);
            sesion.removeAttribute("estanteria");
            estanterias = estanteriasFacade.findAll();
            sesion.setAttribute("estanterias", estanterias);
            response.sendRedirect("/Biblio/estanterias/estanteriasLista.jsp");
        }
        if (accion.equalsIgnoreCase("volverEstanteriasNueva")) {
            response.sendRedirect("/Biblio/estanterias/estanteriasLista.jsp");
        }
        /*
         * Opciones de estanteriasEditar.jsp
         */

        if (accion.equalsIgnoreCase("estanteriasModificar")) {
            signatura = request.getParameter("signatura");
            descripcion = request.getParameter("descripcion");
            imagen = request.getParameter("imagen");
            if (!imagen.equals("")) {
                URL urlImagen = new URL(imagen);
                estanteria.setImagen(urlImagen);
            }
            Date fechaC = new Date();
            estanteria.setSignatura(signatura);
            estanteria.setDescripcion(descripcion);

            estanteria.setFechaM(fechaC);
            estanteriasFacade.edit(estanteria);
            sesion.removeAttribute("estanteria");
            estanterias = estanteriasFacade.findAll();
            sesion.setAttribute("estanterias", estanterias);
            response.sendRedirect("/Biblio/estanterias/estanteriasLista.jsp");
        }
        if (accion.equalsIgnoreCase("volverEstanteriasModificar")) {
            response.sendRedirect("/Biblio/estanterias/estanteriasLista.jsp");
        }
        /*
         * Opciones de estanteriasSeleccionar.jsp
         */
        if (accion.equalsIgnoreCase("seleccionarEstanteria")) {
            setSessionEstanteriaSelect(request);
            String opcion = (String) sesion.getAttribute("selEstanteria");
            sesion.removeAttribute("selEstanteria");
            if (opcion.equalsIgnoreCase("Nueva")) {
                response.sendRedirect("/Biblio/libros/librosNuevo.jsp");
            } else if (opcion.equalsIgnoreCase("Editar")) {
                response.sendRedirect("/Biblio/libros/librosEditar.jsp");
            }

        }

    }

    private void setRequestEstanteriarSelect(HttpServletRequest request) {
        estanterias = (List<Estanterias>) sesion.getAttribute("estanterias");
        estanteria = estanterias.get(sel - 1);
        request.setAttribute("estanteria", estanteria);

    }

    private void setSessionEstanteriaSelect(HttpServletRequest request) {
        estanterias = (List<Estanterias>) sesion.getAttribute("estanterias");
        estanteria = estanterias.get(sel - 1);
        sesion.setAttribute("estanteria", estanteria);

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
