/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package control.servlets;

import control.SesionControl;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.ejb.AutoresFacade;
import model.ejb.EstanteriasFacade;
import model.ejb.LibrosFacade;
import model.ejb.MateriasFacade;
import model.entity.Autores;
import model.entity.Estanterias;
import model.entity.Libros;
import model.entity.Materias;

/**
 *
 * @author p02sotos
 */
public class Index extends HttpServlet {

    @EJB
    private EstanteriasFacade estanteriasFacade;
    SesionControl sesionControl = lookupSesionControlBean();
    @EJB
    private AutoresFacade autoresFacade;
    @EJB
    private MateriasFacade materiasFacade;
    @EJB
    private LibrosFacade librosFacade;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");

        HttpSession sesion = request.getSession();
        String user = request.getRemoteUser();
        sesion.setAttribute("user", user);


        List<Autores> autores = autoresFacade.findAll();
        List<Materias> materias = materiasFacade.findAll();
        List<Libros> libros = librosFacade.findAll();
        List<Estanterias> estanterias = estanteriasFacade.findAll();

        sesionControl.setAutores(autores);
        sesionControl.setLibros(libros);
        sesionControl.setMaterias(materias);







        String accion = request.getParameter("accion");

        if (accion.equalsIgnoreCase("autores")) {
            sesion.setAttribute("autores", autores);
            request.getRequestDispatcher("autores/autoresLista.jsp").forward(request, response);
        }
        if (accion.equalsIgnoreCase("materias")) {
            sesion.setAttribute("materias", materias);
            request.getRequestDispatcher("materias/materiasLista.jsp").forward(request, response);
        }
        if (accion.equalsIgnoreCase("libros")) {
            sesion.setAttribute("libros", libros);
            request.getRequestDispatcher("libros/librosLista.jsp").forward(request, response);
        }
        if (accion.equalsIgnoreCase("estanterias")) {
            sesion.setAttribute("estanterias", estanterias);
            request.getRequestDispatcher("estanterias/estanteriasLista.jsp").forward(request, response);

            request.getRequestDispatcher("/Libros").forward(request, response);
        }
        if (accion.equalsIgnoreCase("libroNuevo")) {

            request.getRequestDispatcher("/Libros").forward(request, response);
        }

        if (accion.equalsIgnoreCase("busquedaAvanzada")) {

            request.getRequestDispatcher("busquedas/busquedaAvanzada.jsp").forward(request, response);
        }
        if (accion.equalsIgnoreCase("cerrarSesion")) {
            if (!sesion.isNew()) {
                sesion.invalidate();
                sesion = request.getSession();
                response.sendRedirect("/Biblio/index.jsp");


            }
        }



    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private SesionControl lookupSesionControlBean() {
        try {
            Context c = new InitialContext();
            return (SesionControl) c.lookup("java:global/Biblio/SesionControl!control.SesionControl");
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
}
