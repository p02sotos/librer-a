/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package control.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import model.ejb.AutoresFacade;
import model.ejb.EstanteriasFacade;
import model.ejb.LibrosFacade;
import model.ejb.MateriasFacade;
import model.entity.Autores;
import model.entity.Estanterias;
import model.entity.Libros;
import model.entity.Materias;

/**
 *
 * @author p02sotos
 */
public class BusquedasServlet extends HttpServlet {
    @EJB
    private EstanteriasFacade estanteriasFacade;

    @EJB
    private MateriasFacade materiasFacade;
    @EJB
    private AutoresFacade autoresFacade;
    @EJB
    private LibrosFacade librosFacade;
    
    private HttpSession sesion;
    private List<Autores> autores = null;
    private List<Libros> libros = null;
    private List<Materias> materias = null;
    private List<Estanterias> estanterias = null;
    private Autores autor = null;
    private Materias materia = null;
    private Estanterias estanteria = null;
    private Libros libro = null;
    private String busqueda;
    private String accion;
    private String buscarPor;
    int sel;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        request.setCharacterEncoding("UTF-8");
        response.setContentType("text/html;charset=UTF-8");
        sesion = request.getSession();

        accion = request.getParameter("accion");
        buscarPor = request.getParameter("buscarPor");
        busqueda = request.getParameter("busqueda");

        String selecion = request.getParameter("seleccion");
        if (selecion != null) {
            try {
                sel = Integer.parseInt(selecion);
            } catch (NumberFormatException e) {
                e.printStackTrace();
            }
        }

        if (accion.equalsIgnoreCase("buscar")) {
            if (buscarPor.equalsIgnoreCase("titulo")) {
                request.setAttribute("busquedaRealizada", "true");
                if (request.getParameter("exacta") == null) {
                    busqueda = "%" + busqueda + "%";
                }
                libros = librosFacade.findLibrosByTitulo(busqueda);
                sesion.setAttribute("resultadoBusqueda", libros);
                request.getRequestDispatcher("/index.jsp").forward(request, response);

            }
            if (buscarPor.equalsIgnoreCase("isbn")) {
                request.setAttribute("busquedaRealizada", "true");
                if (request.getParameter("exacta") == null) {
                    busqueda = "%" + busqueda + "%";
                }
                libros = librosFacade.findLibrosByIsbn(busqueda);
                sesion.setAttribute("resultadoBusqueda", libros);
                request.getRequestDispatcher("/index.jsp").forward(request, response);
            }
            if (buscarPor.equalsIgnoreCase("autor")) {
                request.setAttribute("busquedaRealizada", "true");
                if (request.getParameter("exacta") == null) {
                    busqueda = "%" + busqueda + "%";
                }
                libros = librosFacade.findLibrosByAutor(busqueda);
                sesion.setAttribute("resultadoBusqueda", libros);
                request.getRequestDispatcher("/index.jsp").forward(request, response);
            }
            
            //SOlucionar esta busqueda, algoi raro hay, tengo que buscar por Estanterias
            if (buscarPor.equalsIgnoreCase("estanteria")) {
                request.setAttribute("busquedaRealizada", "true");
                if (request.getParameter("exacta") == null) {
                    busqueda = "%" + busqueda + "%";
                }
                
                libros = librosFacade.findLibrosByEstanteriaSignatura(busqueda);
                sesion.setAttribute("resultadoBusqueda", libros);
                request.getRequestDispatcher("/index.jsp").forward(request, response);
            }
            if (buscarPor.equalsIgnoreCase("materia")) {
                request.setAttribute("busquedaRealizada", "true");
                if (request.getParameter("exacta") == null) {
                    busqueda = "%" + busqueda + "%";
                }
                libros = librosFacade.findLibrosByMateria(busqueda);
                sesion.setAttribute("resultadoBusqueda", libros);
                request.getRequestDispatcher("/index.jsp").forward(request, response);

            }


        }
        if (accion.equalsIgnoreCase("busquedaAvanzada")) {
            sesion.removeAttribute("resultadoBusqueda");

            StringBuffer buffer = new StringBuffer();
            boolean vacio = false;

            String criterio1 = request.getParameter("criterio1");
            String criterio2 = request.getParameter("criterio2");
            String criterio3 = request.getParameter("criterio3");
            String exacta1 = request.getParameter("exacta1");
            String exacta2 = request.getParameter("exacta2");
            String exacta3 = request.getParameter("exacta3");
            String boolean1 = request.getParameter("boolean1");
            String boolean2 = request.getParameter("boolean2");

            String buscarP1 = request.getParameter("buscarPor1");
            String buscarP2 = request.getParameter("buscarPor2");
            String buscarP3 = request.getParameter("buscarPor3");


            buffer.append("SELECT DISTINCT l FROM Libros l ");
            if (buscarP1.equalsIgnoreCase("materia") || buscarP2.equalsIgnoreCase("materia") || buscarP3.equalsIgnoreCase("materia")) {
                buffer.append(" JOIN l.materias m ");
            }
            if (buscarP1.equalsIgnoreCase("autor") || buscarP2.equalsIgnoreCase("autor") || buscarP3.equalsIgnoreCase("autor")) {
                buffer.append(" JOIN l.autores a ");
            }
            buffer.append(" WHERE ");


            if (!criterio1.equals("")) {

                buffer.append(compruebaCriterio(buscarP1));
                if (request.getParameter("exacta1") == null) {
                    buffer.append("'%");
                    buffer.append(criterio1);
                    buffer.append("%'");
                } else {
                    buffer.append("'");
                    buffer.append(criterio1);
                    buffer.append("'");
                }

            } else {
                vacio = true;
            }

            if (!criterio2.equals("")) {
                if (!vacio) {
                    buffer.append(compruebaBoolean(boolean1));
                }
                vacio = false;

                buffer.append(compruebaCriterio(buscarP2));
                if (request.getParameter("exacta2") == null) {
                    buffer.append("'%");
                    buffer.append(criterio2);
                    buffer.append("%'");
                } else {
                    buffer.append("'");
                    buffer.append(criterio2);
                    buffer.append("'");
                }


            } else {
                vacio = true;
            }
            if (!criterio3.equals("")) {
                if (!vacio) {
                    buffer.append(compruebaBoolean(boolean2));
                }
                vacio = false;
                buffer.append(compruebaCriterio(buscarP3));
                if (request.getParameter("exacta3") == null) {
                    buffer.append("'%");
                    buffer.append(criterio3);
                    buffer.append("%'");
                } else {
                    buffer.append("'");
                    buffer.append(criterio3);
                    buffer.append("'");
                }

            }
            libros = librosFacade.findLibrosAvanced(buffer.toString());
            request.setAttribute("busquedaRealizada", "true");
            sesion.setAttribute("resultadoBusqueda", libros);
            sesion.setAttribute("libros", libros);
            request.getRequestDispatcher("/busquedas/busquedaAvanzada.jsp").forward(request, response);


        }
        if (accion.equalsIgnoreCase("buscarPorMateria")) {
            materias = (List<Materias>) sesion.getAttribute("materias");
            materia = materias.get(sel - 1);
            libros = librosFacade.findLibrosByMateria(materia.getNombre());
            if (libros.isEmpty()) {
                request.setAttribute("sinResultadoMaterias", "true");
                sesion.setAttribute("materia", materia);
            } else {
                request.setAttribute("busquedaRealizada", "true");
                sesion.setAttribute("resultadoBusqueda", libros);
                sesion.setAttribute("libros", libros);
            }
            request.getRequestDispatcher("/busquedas/busquedasSimples.jsp").forward(request, response);
        }

        if (accion.equalsIgnoreCase("confirmarBorrarMaterias")) {
            materia = (Materias) sesion.getAttribute("materia");
            materiasFacade.remove(materia);
            materias = materiasFacade.findAll();
            sesion.setAttribute("materias", materias);
            response.sendRedirect("/Biblio/materias/materiasLista.jsp");

        }
        if (accion.equalsIgnoreCase("cancelarBorrarMaterias")) {
            sesion.removeAttribute("materia");
            response.sendRedirect("/Biblio/materias/materiasLista.jsp");

        }
        if (accion.equalsIgnoreCase("buscarPorAutor")) {
            autores = (List<Autores>) sesion.getAttribute("autores");
            autor = autores.get(sel - 1);
            libros = librosFacade.findLibrosByAutor(autor.getNormalizado());
            if (libros.isEmpty()) {
                request.setAttribute("sinResultadoAutores", "true");
                sesion.setAttribute("autor", autor);
            } else {
                request.setAttribute("busquedaRealizada", "true");
                sesion.setAttribute("resultadoBusqueda", libros);
                sesion.setAttribute("libros", libros);
            }
            request.getRequestDispatcher("/busquedas/busquedasSimples.jsp").forward(request, response);
        }

        if (accion.equalsIgnoreCase("confirmarBorrarAutores")) {
            autor = (Autores) sesion.getAttribute("autor");
            autoresFacade.remove(autor);
            autores = autoresFacade.findAll();
            sesion.setAttribute("autores", autores);
            response.sendRedirect("/Biblio/autores/autoresLista.jsp");

        }
        if (accion.equalsIgnoreCase("cancelarBorrarAutores")) {
            sesion.removeAttribute("autor");
            response.sendRedirect("/Biblio/autores/autoresLista.jsp");

        }
        if (accion.equalsIgnoreCase("buscarPorEstanteria")) {
            estanterias = (List<Estanterias>) sesion.getAttribute("estanterias");
            estanteria = estanterias.get(sel - 1);
            libros = librosFacade.findLibrosByEstanteriaSignatura(estanteria.getSignatura());
            if (libros.isEmpty()) {
                request.setAttribute("sinResultadoEstanterias", "true");
                sesion.setAttribute("estanteria", estanteria);
            } else {
                request.setAttribute("busquedaRealizada", "true");
                sesion.setAttribute("resultadoBusqueda", libros);
                sesion.setAttribute("libros", libros);
            }
            request.getRequestDispatcher("/busquedas/busquedasSimples.jsp").forward(request, response);
        }
         if (accion.equalsIgnoreCase("confirmarBorrarEstanterias")) {
            estanteria = (Estanterias) sesion.getAttribute("estanteria");
            estanteriasFacade.remove(estanteria);
            estanterias = estanteriasFacade.findAll();
            sesion.setAttribute("estanterias", estanterias);
            response.sendRedirect("/Biblio/estanterias/estanteriasLista.jsp");

        }
        if (accion.equalsIgnoreCase("cancelarBorrarEstanterias")) {
            sesion.removeAttribute("estanteria");
            response.sendRedirect("/Biblio/estanterias/estanteriasLista.jsp");

        }


    }

    private String compruebaCriterio(String criterio) {
        String resultado = "";
        if (criterio.equalsIgnoreCase("titulo")) {
            resultado = "l.titulo LIKE ";
        }
        if (criterio.equalsIgnoreCase("autor")) {
            resultado = "a.normalizado LIKE ";
        }
        if (criterio.equalsIgnoreCase("materia")) {
            resultado = "m.nombre LIKE ";
        }
        if (criterio.equalsIgnoreCase("isbn")) {
            resultado = "l.isbn LIKE ";
        }
        if (criterio.equalsIgnoreCase("estanteria")) {
            resultado = "l.estanteria.signatura LIKE ";
        }
        return resultado;

    }

    private String compruebaBoolean(String bool) {
        if (bool.equalsIgnoreCase("OR")) {
            return " OR ";
        }
        if (bool.equalsIgnoreCase("AND")) {
            return " AND ";
        }
        if (bool.equalsIgnoreCase("NOT")) {
            return " NOT ";
        }
        return "";

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
