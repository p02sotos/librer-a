<%-- 
    Document   : busquedaAvanzada
    Created on : 30-jul-2013, 11:56:41
    Author     : biblioteca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Busqueda</title>
        <%@include file="../WEB-INF/jspf/includes.jspf" %>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#TablaResultados, #TablaResultadosBusqueda').dataTable({
                    "oLanguage": {
                        "sUrl": "/Biblio/resources/traduccion.txt"
                    }
                });
            });
        </script>     
    </head>
    <body>        
        <%@include file="../WEB-INF/jspf/header.jspf" %>
        <h1>Búsqueda Avanzada</h1>
        <boot:cuerpo id="busquedaAvanzada" clase="span12">
            <div class="span3"></div>
            <div class="span6">
                <form class="form-search" action="/Biblio/Busquedas" method="POST">
                    <p>
                        <input type="text" class="input-large span4" name="criterio1">
                        <select name="buscarPor1" class="span2">
                            <option value="titulo">Título</option>
                            <option value="isbn">Isbn</option>
                            <option value="autor">Autor</option>
                            <option value="materia">Materia</option>
                            <option value="estanteria">Estanteria</option>
                        </select><br/>
                        <label style ="padding-left: 30px;" class="checkbox "><input type="checkbox" name="exacta1">Búsqueda exacta</label>
                        <label style ="padding-left: 30px;" class="checkbox "><input type="radio" name="boolean1" value="AND">AND</label>
                        <label style ="padding-left: 30px;" class="checkbox "><input type="radio" name="boolean1" value="OR" checked>OR</label>
                        <label style ="padding-left: 30px;" class="checkbox "><input type="radio" name="boolean2" value="NOT" checked>NOT</label><br/>

                    </p>
                    <p>
                        <input type="text" class="input-large span4" name="criterio2">
                        <select name="buscarPor2" class="span2">
                            <option value="titulo">Título</option>
                            <option value="isbn">Isbn</option>
                            <option value="autor">Autor</option>
                            <option value="materia">Materia</option>
                            <option value="estanteria">Estanteria</option>
                        </select><br/>
                        <label style ="padding-left: 30px;"  class="checkbox "><input type="checkbox" name="exacta2">Búsqueda exacta</label>
                        <label style ="padding-left: 30px;" class="checkbox "><input type="radio" name="boolean2" value="AND">AND</label>
                        <label style ="padding-left: 30px;" class="checkbox "><input type="radio" name="boolean2" value="OR" checked>OR</label>
                        <label style ="padding-left: 30px;" class="checkbox "><input type="radio" name="boolean2" value="NOT" checked>NOT</label><br/>
                    </p>
                    <p>
                        <input type="text" class="input-large span4" name="criterio3">
                        <select name="buscarPor3" class="span2">
                            <option value="titulo">Título</option>
                            <option value="isbn">Isbn</option>
                            <option value="autor">Autor</option>
                            <option value="materia">Materia</option>
                             <option value="estanteria">Estanteria</option>
                        </select><br/>
                        <label style ="padding-left: 30px;" class="checkbox "><input type="checkbox" name="exacta3">Búsqueda exacta</label>

                    </p>
                    <button type="submit" name="accion" value="busquedaAvanzada" class="btn btn-primary pull-right">Buscar</button>
                </form>

                <c:if test="${requestScope.busquedaRealizada}">
                    <%@include file="../WEB-INF/jspf/busquedaGeneral.jspf" %>
                </c:if>
            </div>
            <div class="span3"></div>
        </boot:cuerpo>      
    </body>
</html>
