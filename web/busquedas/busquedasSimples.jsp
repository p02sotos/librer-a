<%-- 
    Document   : busquedaAvanzada
    Created on : 30-jul-2013, 11:56:41
    Author     : biblioteca
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Administrar Campos</title>
        <%@include file="../WEB-INF/jspf/includes.jspf" %>
         
    </head>
    <body>        
        <%@include file="../WEB-INF/jspf/header.jspf" %>

        <boot:cuerpo id="busquedaSimple" clase="span12">
            <div class="span2"></div>
            <div class="span8">
                <h1>Búsqueda Individual</h1>
                <c:if test="${busquedaRealizada}">
                    <%@include file="../WEB-INF/jspf/busquedaGeneral.jspf" %>
                    <p>
                        Si desea borrar este autor o materia debe eliminarlo de los
                        libros asociados
                    </p>
                </c:if>
                <c:if test="${sinResultadoMaterias}">
                    <boot:accionForm servlet="Busquedas">
                        <div class="alert alert-block">
                            <p class="text-center">
                                No existe ningún libro con esta materia asociada<br/>
                                ¿DESEA BORRARLA?                            
                            </p>
                            <p class="text-center">
                                <button class="btn btn-small btn-info" type="submit" value="confirmarBorrarMaterias" name="accion" title="Confirmar" >Confirmar </button>
                                <button class="btn btn-small btn-danger" type="submit" value="cancelarBorrarMaterias" name="accion" title="Confirmar" >Cancelar </button>
                            </p>
                        </div>
                    </boot:accionForm>           

                </c:if>
                <c:if test="${sinResultadoAutores}">
                    <boot:accionForm servlet="Busquedas">
                        <div class="alert alert-block">
                            <p class="text-center">
                                No existe ningún libro con ese autor asociado<br/>
                                ¿DESEA BORRARLA?                            
                            </p>
                            <p class="text-center">
                                <button class="btn btn-small btn-info" type="submit" value="confirmarBorrarAutores" name="accion" title="Confirmar" >Confirmar </button>
                                <button class="btn btn-small btn-danger" type="submit" value="cancelarBorrarAutores" name="accion" title="Cancelar" >Cancelar </button>
                            </p>
                        </div>
                    </boot:accionForm>          

                </c:if>
                    <c:if test="${sinResultadoEstanterias}">
                    <boot:accionForm servlet="Busquedas">
                        <div class="alert alert-block">
                            <p class="text-center">
                                No existe ningún libro con esa estanteria asociada<br/>
                                ¿DESEA BORRARLA?                            
                            </p>
                            <p class="text-center">
                                <button class="btn btn-small btn-info" type="submit" value="confirmarBorrarEstanterias" name="accion" title="Confirmar" >Confirmar </button>
                                <button class="btn btn-small btn-danger" type="submit" value="cancelarBorrarEstanterias" name="accion" title="Cancelar" >Cancelar </button>
                            </p>
                        </div>
                    </boot:accionForm>          

                </c:if>
            </div>
            <div class="span2"></div>

        </boot:cuerpo>      
    </body>
</html>
