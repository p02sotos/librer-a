<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Estanterías</title>
        <%@include file="../WEB-INF/jspf/includes.jspf" %>
         <script type="text/javascript">
            $(document).ready(function() {
                $('#TablaResultados').dataTable({
                    "oLanguage": {
                        "sUrl": "/Biblio/resources/traduccion.txt"
                    }
                });
            });
        </script>     
    </head>
    <body>
        <%@include file="../WEB-INF/jspf/header.jspf" %>
        <boot:cuerpo id="estanteriasLista">
            <div class="span2"></div>
            <div class="span8">
                <h1>Estanterías</h1>               
                    <boot:accionForm servlet="Estanterias">
                        <button type="submit" name="accion" class="btn btn-primary" value="estanteriaNueva">Nueva Estanteria</button>
                    </boot:accionForm>
                    <table border="0" class="table table-condensed" id="TablaResultados">
                        <thead>
                            <tr>                                     
                                <th class="columnId">Id</th>
                                <th class="columnPrincipal">Signatura</th>
                                <th class="column2">Descripción</th>
                                <th>Creacion</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="actual" items="${estanterias}" varStatus="actualStatus">
                                <tr>
                                    <td><c:out value="${actual.id}"/></td>
                                    <td><c:out value="${actual.signatura}"/></td>
                                    <td><c:out value="${actual.descripcion}"/></td>
                                    <td><c:out value="${actual.fechaC}"/></td>
                                    <td class="botones-crud">  <form class="btn-group pull-left" id ="formulario" action="/Biblio/Estanterias" method="GET"> 
                                            <input class="btn btn-small " type="hidden" name="seleccion" value="${actualStatus.count}"/>
                                            <!--<button class="btn btn-small" type="submit" value="autorVer" name="accion" title="Ver" />
                                            <span class="add-on"><i class="icon-eye-open"></i></span> 
                                            </button>-->
                                            <button class="btn btn-small btn-info" type="submit" value="estanteriasEditar" name="accion" title="Editar" />
                                            <span class="add-on"><i class="icon-pencil"></i></span> 
                                            </button></form>
                                        <form class="btn-group pull-left" id ="formulario" action="/Biblio/Busquedas" method="GET"> 
                                            <input class="btn btn-small " type="hidden" name="seleccion" value="${actualStatus.count}"/>
                                            <button class="btn btn-warning btn-small" type="submit" value="buscarPorEstanteria" name="accion" title="Buscar" />
                                            <span class="add-on"><i class="icon-search"></i></span> 
                                            </button>
                                        </form>
                                    </td>
                                </c:forEach>
                        </tbody>
                    </table>               
            </div>
            <div class="span2"></div>     
        </boot:cuerpo>
    </body>
</html>
