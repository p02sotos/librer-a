<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Selecciona Estanteria a colocar</title>
        <%@include file="../WEB-INF/jspf/includes.jspf" %>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#TablaResultados').dataTable({
                    "oLanguage": {
                        "sUrl": "/Biblio/resources/traduccion.txt"
                    }
                });
            });
        </script>     
    </head>
    <body>
        <%@include file="../WEB-INF/jspf/header.jspf" %>
        <boot:cuerpo id="estanteriasSeleccion">
            <div class="span2"></div>
            <div class="span8">
                <h1>Seleccionar Estanteria</h1>               
                <table border="0" class="table table-condensed" id="TablaResultados">
                    <thead>
                        <tr>                                     
                            <th class="columnId">Id</th>
                            <th class="columnPrincipal">Signatura</th>
                            <th class="column2">Descripción</th>
                            <th>Creacion</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="actual" items="${estanterias}" varStatus="actualStatus">
                            <tr>
                                <td><c:out value="${actual.id}"/></td>
                                <td><c:out value="${actual.signatura}"/></td>
                                <td><c:out value="${actual.descripcion}"/></td>
                                <td><c:out value="${actual.fechaC}"/></td>
                                <td class="botones-crud">  
                                    <form class="btn-group pull-left" id ="formulario" action="/Biblio/Estanterias" method="GET"> 
                                        <input class="btn btn-small " type="hidden" name="seleccion" value="${actualStatus.count}"/>
                                        <!--<button class="btn btn-small" type="submit" value="autorVer" name="accion" title="Ver" />
                                        <span class="add-on"><i class="icon-eye-open"></i></span> 
                                        </button>-->
                                        <button class="btn btn-small btn-info" type="submit" value="seleccionarEstanteria" name="accion" title="Seleccionar" />
                                        <span class="add-on"><i class="icon-check"></i></span> 
                                        </button>
                                    </form>

                                </td>
                            </c:forEach>
                    </tbody>
                </table>               
            </div>
            <div class="span2"></div>     
        </boot:cuerpo>
    </body>
</html>
