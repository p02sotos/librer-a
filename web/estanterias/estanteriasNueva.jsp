<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nueva Estanteria</title>
        <%@include file="../WEB-INF/jspf/includes.jspf" %>
    </head>    
    <body>
        <%@include file="../WEB-INF/jspf/header.jspf" %>
        <boot:cuerpo id="estanteriasNueva">
            <div class="span2"></div>
            <div class="span8">
                <h2>Nueva Estantería</h2>
                <boot:accionForm servlet="Estanterias" id="validar">
                    <boot:input nombre="signatura" label="Signatura " id="signatura" />
                    <boot:input nombre="descripcion" label="Descripción" id="descripcion" />
                    <boot:input nombre="imagen" label="Imagen (indicar la url)" id="imagen" />
                    <boot:accionButton tipo="submit" clase="btn-primary" etiqueta="Guardar" valor="guardarEstanteria" />
                </boot:accionForm>
                <boot:accionForm servlet="Estanterias">
                <boot:accionButton tipo="submit"  etiqueta="Volver" valor="volverEstanteriaNueva" />
                </boot:accionForm>

            </div>
            <div class="span2"></div>
        </boot:cuerpo>
    </body>
</html>
