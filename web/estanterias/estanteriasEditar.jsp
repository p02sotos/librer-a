<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar estantería</title>
        <%@include file="../WEB-INF/jspf/includes.jspf" %>
    </head>    
    <body>
        <%@include file="../WEB-INF/jspf/header.jspf" %>
        <boot:cuerpo id="estanteriasEditar">
            <div class="span2"></div>
            <div class="span8">
                <h2>Editar estantería</h2>
                <boot:accionForm servlet="Estanterias" id="validar">
                    <boot:input nombre="signatura" label="Signatura " id="signatura" valor="${estanteria.signatura}" />
                    <boot:input nombre="descripcion" label="Descripción" id="descripcion" valor="${estanteria.descripcion}" />
                    <boot:input nombre="imagen" label="Imagen (indicar la url)" id="imagen" valor="${estanteria.imagen}" />
                    <boot:accionButton tipo="submit" clase="btn-primary" etiqueta="Modificar" valor="estanteriasModificar" />
                </boot:accionForm>
                <boot:accionForm servlet="Estanterias">
                    <boot:accionButton tipo="submit"  etiqueta="Volver" valor="volverEstanteriasModificar" />
                </boot:accionForm>

            </div>
            <div class="span2"></div>
        </boot:cuerpo>
    </body>
</html>