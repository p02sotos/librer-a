<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ver Autor</title>
        <%@include file="../WEB-INF/jspf/includes.jspf" %>
    </head>    
    <body>
        <%@include file="../WEB-INF/jspf/header.jspf" %>
        <boot:cuerpo id="autoresEditar">
            <h2>Ver Autor</h2>
            <boot:accionForm servlet="Autores">
                <boot:input nombre="normalizado" desactivado="true" label="Término Normalizado" id="normalizado" valor="${autor.normalizado}" />
                <boot:input nombre="variantes" desactivado="true" label="Variante" id="variante"  valor="${autor.variantes}"/>
                <boot:input nombre="usadoPor" desactivado="true" label="Usado Por" id="usadoPor"  valor="${autor.usadoPor}" />                
                <boot:accionButton tipo="submit"  etiqueta="Volver" valor="volverAutorVer" />
            </boot:accionForm>
        </boot:cuerpo>
    </body>
</html>