<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nuevo Autor</title>
        <%@include file="../WEB-INF/jspf/includes.jspf" %>
    </head>    
    <body>
        <%@include file="../WEB-INF/jspf/header.jspf" %>
        <boot:cuerpo id="autoresNuevo">
            <div class="span2"></div>
            <div class="span8">
                <h2>Nuevo Autor</h2>
                <boot:accionForm servlet="Autores" id="validar">
                    <boot:input nombre="normalizado" label="Término Normalizado" id="normalizado" />
                    <boot:input nombre="variantes" label="Variante" id="variante" />
                    <boot:input nombre="usadoPor" label="Usado Por" id="usadoPor" />
                    <boot:accionButton tipo="submit" clase="btn-primary" etiqueta="Guardar" valor="guardarAutor" />
                </boot:accionForm>
                <boot:accionForm servlet="Autores">
                <boot:accionButton tipo="submit"  etiqueta="Volver" valor="volverAutorNuevo" />
                </boot:accionForm>

            </div>
            <div class="span2"></div>
        </boot:cuerpo>
    </body>
</html>
