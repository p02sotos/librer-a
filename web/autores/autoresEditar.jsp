<%-- 
    Document   : autoresEditar
    Created on : 15-jul-2013, 22:05:20
    Author     : p02sotos
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar autores</title>
        <%@include file="../WEB-INF/jspf/includes.jspf" %>
    </head>    
    <body>
        <%@include file="../WEB-INF/jspf/header.jspf" %>
        <boot:cuerpo id="autoresEditar">
            <div class="span2"></div>
            <div class="span8">
                <h2>Editar autores</h2>
                <boot:accionForm servlet="Autores" id="validar">
                    <boot:input nombre="normalizado" label="Término Normalizado" id="normalizado" valor="${autor.normalizado}" />
                    <boot:input nombre="variantes" label="Variante" id="variante"  valor="${autor.variantes}"/>
                    <boot:input nombre="usadoPor" label="Usado Por" id="usadoPor"  valor="${autor.usadoPor}" />
                    <boot:accionButton tipo="submit" clase="btn-primary" etiqueta="Modificar" valor="autorModificar" />
                </boot:accionForm>
                <boot:accionForm servlet="Autores">
                    <boot:accionButton tipo="submit"  etiqueta="Volver" valor="volverAutorEditar" />
                </boot:accionForm>

            </div>
            <div class="span2"></div>
        </boot:cuerpo>
    </body>
</html>