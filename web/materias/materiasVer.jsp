<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ver Materia</title>
        <%@include file="../WEB-INF/jspf/includes.jspf" %>
    </head>    
    <body>
        <%@include file="../WEB-INF/jspf/header.jspf" %>
        <boot:cuerpo id="materiasVer">
            <div class="span2"></div>
            <div class="span8">
                <h2>Ver Materia</h2>
                <boot:accionForm servlet="Materias">
                    <boot:input nombre="nombre" desactivado="true" label="Nombre" id="nombre" valor="${materia.nombre}" />
                    <boot:input nombre="descripcion" desactivado="true" label="Término Normalizado" id="normalizado" valor="${materia.descripcion}" />
                    <boot:input nombre="vease" desactivado="true" label="Variante" id="variante"  valor="${materia.vease}"/>
                    <boot:input nombre="usadoPor" desactivado="true" label="Usado Por" id="usadoPor"  valor="${materia.usadoPor}" />
                    <boot:input nombre="fechaCreacion" desactivado="true" label="Fecha de Creacion Por" id="fechaCreacion"  valor="${materia.fechaCreacion}" />
                    <boot:input nombre="fechaModificacion" desactivado="true" label="Fecha Modificación" id="fechaMOdificacion"  valor="${materia.fechaModificacion}" />
                    <boot:accionButton tipo="submit"  etiqueta="Volver" valor="volverMateriaVer" />
                </boot:accionForm>
            </div>
            <div class="span2"></div>
        </boot:cuerpo>
    </body>
</html>