<%-- 
    Document   : materiaesEditar
    Created on : 15-jul-2013, 22:05:20
    Author     : p02sotos
--%>


<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar Materia</title>
        <%@include file="../WEB-INF/jspf/includes.jspf" %>
    </head>    
    <body>
        <%@include file="../WEB-INF/jspf/header.jspf" %>
        <boot:cuerpo id="materiasEditar">
            <div class="span2"></div>
            <div class="span8">
                <h2>Editar Materia</h2>
                <boot:accionForm servlet="Materias" id="validar">
                    <boot:input nombre="nombre" label="Nombre" id="nombre" valor="${materia.nombre}" />
                    <boot:input nombre="descripcion" label="Descripcion" id="descripcion" valor="${materia.descripcion}" />
                    <boot:input nombre="vease" label="Véase" id="vease"  valor="${materia.vease}"/>
                    <boot:input nombre="usadoPor" label="Usado Por" id="usadoPor"  valor="${materia.usadoPor}" />
                    <boot:accionButton tipo="submit" clase="btn-primary" etiqueta="Modificar" valor="materiaModificar" />
                </boot:accionForm>
                <boot:accionForm servlet="Materias" >
                    <boot:accionButton tipo="submit"  etiqueta="Volver" valor="volverMateriaEditar" />
                </boot:accionForm>
            </div>
            <div class="span2"></div>
        </boot:cuerpo>
    </body>
</html>