<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nueva Materia</title>
        <%@include file="../WEB-INF/jspf/includes.jspf" %>
    </head>    
    <body>
        <%@include file="../WEB-INF/jspf/header.jspf" %>
        <boot:cuerpo id="materiasNueva">
            <div class="span2"></div>
            <div class="span8">
                <h2>Nueva Materia</h2>
                <boot:accionForm servlet="Materias" id="validar">
                    <boot:input nombre="nombre" label="Nombre" id="nombre" />
                    <boot:input nombre="descripcion" label="Descripcion" id="normalizado" />
                    <boot:input nombre="vease" label="Vease" id="variante"  />
                    <boot:input nombre="usadoPor" label="Usado Por" id="usadoPor"  />
                    <boot:accionButton tipo="submit" clase="btn-primary" etiqueta="Guardar" valor="guardarMateria" />
                </boot:accionForm>
                <boot:accionForm servlet="Materias">
                    <boot:accionButton tipo="submit"  etiqueta="Volver" valor="volverMateriaNuevo" />
                </boot:accionForm>
            </div>
            <div class="span2"></div>
        </boot:cuerpo>
    </body>
</html>
