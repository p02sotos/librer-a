<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Materias</title>
        <%@include file="../WEB-INF/jspf/includes.jspf" %>
         <script type="text/javascript">
            $(document).ready(function() {
                $('#TablaResultados').dataTable({
                    "oLanguage": {
                        "sUrl": "/Biblio/resources/traduccion.txt"
                    }
                });
            });
        </script>     
    </head>
    <body>
        <%@include file="../WEB-INF/jspf/header.jspf" %>
        <boot:cuerpo id="materiasLista">
            <div class="span2"></div>
            <div class="span8">
                <h1>Materias</h1>
                <boot:accionForm servlet="Materias">
                       <button type="submit" name="accion" class="btn btn-primary" value="materiaNuevo">Nueva Materia</button>
                </boot:accionForm>
                <table border="0" class="table table-condensed" id="TablaResultados">
                    <thead>
                        <tr>
                            <th>Materia</th>
                            <th>Descripcion</th>
                            <th><span>Usado Por</span></th>
                            <th>Creacion</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="actual" items="${materias}" varStatus="actualStatus">
                            <tr>
                                <td><c:out value="${actual.nombre}"/></td>
                                <td><c:out value="${actual.descripcion}"/></td>
                               
                                <td><c:out value="${actual.usadoPor}"/></td>
                                <td><c:out value="${actual.fechaCreacion}"/></td>
                                <td class="botones-crud">  <form class="btn-group pull-left" id ="formulario" action="/Biblio/Materias" method="GET"> 
                                        <input class="btn btn-small " type="hidden" name="seleccion" value="${actualStatus.count}"/>
                                        <button class="btn btn-small" type="submit" value="materiaVer" name="accion" title="Ver" >
                                            <span class="add-on"><i class="icon-eye-open"></i></span> 
                                        </button>
                                        <button class="btn btn-small btn-info" type="submit" value="materiaEditar" name="accion" title="Editar" >
                                            <span class="add-on"><i class="icon-pencil"></i></span> 
                                        </button> </form>
                                    <form  id ="formulario" action="/Biblio/Busquedas" method="GET">
                                        <input class="btn btn-small " type="hidden" name="seleccion" value="${actualStatus.count}"/>
                                        <button class="btn btn-warning btn-small" type="submit" value="buscarPorMateria" name="accion" title="Buscar por materia" >
                                            <span class="add-on"><i class="icon-search"></i></span> 
                                        </button> </form>
                                </td>
                            </c:forEach>
                    </tbody>
                </table>
                <c:if test="${requestScope.confirmar}">
                    <boot:accionForm servlet="Materias">
                        <div class="alert alert-block">
                            <p class="text-center">
                                ¿Seguro que desea borrar?
                            </p>
                            <p class="text-center">
                                <input class="btn" type="submit" value="Confirmar" name="accion" />
                                <input class="btn" type="submit" value="Cancelar" name="accion" />
                            </p>
                        </div>
                    </boot:accionForm>
                </c:if>
            </div>
            <div class="span2"></div>
        </boot:cuerpo>
    </body>
</html>
