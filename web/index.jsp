<%-- 
    Document   : index
    Created on : 15-jul-2013, 18:56:05
    Author     : p02sotos
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib tagdir="/WEB-INF/tags/" prefix="boot" %>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%@include file="WEB-INF/jspf/includes.jspf" %>
        <title>Librería Personal</title>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#TablaResultados, #TablaResultadosBusqueda').dataTable({
                    "oLanguage": {
                        "sUrl": "/Biblio/resources/traduccion.txt"
                    }
                });
            });
        </script>     
    </head>
    <body> 
        <%@include file="WEB-INF/jspf/header.jspf" %>

        <boot:cuerpo id="indice">
            <div class="span2"></div>
            <div class="span8">
            <h3>Introduce tu busqueda</h3>
            <boot:inputSearch nombre="busqueda" servlet="Busquedas" />
            <c:if test="${requestScope.busquedaRealizada}">
                <%@include file="WEB-INF/jspf/busquedaGeneral.jspf" %>
            </c:if>
            </div>
            <div class="span2"></div>
            Usuario ${user}
        </boot:cuerpo>

    </body>
</html>
