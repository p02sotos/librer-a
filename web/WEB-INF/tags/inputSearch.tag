<%@tag description="Input con botón de búsqueda" pageEncoding="UTF-8"%>
<%@attribute name="nombre"%>
<%@attribute name="servlet"%>
<form class="form-search" action="/Biblio/${servlet}" method="POST">
    <input type="text" class="input-large span4" name="${nombre}">
    <select name="buscarPor" class="span2">
        <option value="titulo">Título</option>
        <option value="isbn">Isbn</option>
        <option value="autor">Autor</option>
        <option value="materia">Materia</option>
        <option value="estanteria">Estanteria</option>
    </select>
    <button type="submit" name="accion" value="buscar" class="btn btn-primary">Buscar</button>
     <br/><label style ="padding-left: 30px;"class="checkbox "><input type="checkbox" name="exacta">Búsqueda exacta</label>
</form>