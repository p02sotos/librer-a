<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%-- The list of normal or fragment attributes can be specified here: --%>
<%-- any content can be specified here e.g.: --%>
<%@attribute name="label"%>
<%@attribute name="desactivado"%>
<%@attribute name="nombre" required="true"%>
<%@attribute name="valor"%>
<%@attribute name="clase"%>
<%@attribute name="id"%>

<%-- any content can be specified here e.g.: --%>
<div class="control-group">
    <label class="control-label" for="${id}">${label}</label>
    <div class="controls">
        <input class="${clase}" <c:if test="${desactivado}"> readonly disabled
               </c:if> type="text" id="${id}" name="${nombre}" value="${valor}" />
    </div>
</div>