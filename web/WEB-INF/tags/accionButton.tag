<%@tag  display-name="button" description="Botón de acción genérico de aplicacion CRUD con Bootstrap" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%-- The list of normal or fragment attributes can be specified here: --%>
<%@attribute name="label"%>
<%@attribute name="desactivado"%>
<%@attribute name="etiqueta"%>
<%@attribute name="valor"%>
<%@attribute name="clase"%>
<%@attribute name="tipo" required="true"%>
<%@attribute name="id"%>
<div class="control-group">
    <c:if test="${label}">
        <label class="control-label" for="${id}">${label}</label> 
    </c:if>
    <div class="controls">
        <button class="${clase} btn" <c:if test="${desactivado}"> readonly disabled
                </c:if> type="${tipo}" id="${id}" name="accion" value="${valor}">
            ${etiqueta}
        </button>
    </div>
</div>