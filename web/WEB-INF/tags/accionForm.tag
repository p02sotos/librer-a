<%@tag description="put the tag description here" pageEncoding="UTF-8" body-content="scriptless"%>
<%@attribute name="servlet" required="true"%>
<%@attribute name="id"%>
<form class="form-horizontal" id ="${id}" action="/Biblio/${servlet}" method="POST">
    <jsp:doBody  />
</form>