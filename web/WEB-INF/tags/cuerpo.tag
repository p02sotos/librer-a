<%@tag description="put the tag description here" pageEncoding="UTF-8"%>
<%@attribute name="id" required="true"%>
<%@attribute name="clase" %>
<div id="contenido" class="container">
    <div class="row">
        <div id="${id}" class="${clase}">
            <jsp:doBody />
        </div> 
    </div>    
</div>
