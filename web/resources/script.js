/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

$().ready(function() {


    $("#validar").validate({
        rules: {
            titulo: {
                required: true,
                minlength: 2

            },
            normalizado: {
                required: true,
                minlength: 2
            },
            nombre: {
                required: true,
                minlength: 2
            },
            signatura: {
                required: true,
                minlenght: 1
            }
        },
        messages: {
            nombre: 'Debe ingresar el nombre',
            normalizado: 'Debe ingresar el término normalizado',
            titulo: 'El libro como mínimo debe tener un título'
        }
    });
   
    

});
