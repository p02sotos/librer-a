<%-- 
    Document   : login
    Created on : 24-ago-2013, 18:25:54
    Author     : p02sotos
--%>
<%@taglib prefix="boot" tagdir="/WEB-INF/tags/" %>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ingreso en la Biblioteca</title>
        <script src="http://code.jquery.com/jquery-1.10.1.min.js"></script>
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
        <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.no-icons.min.css" rel="stylesheet">

    </head>
    <body>
        <boot:cuerpo id="login">
            <h1 style="text-align: center">Ingrese usuario y contraseña</h1>
            <div class="span3"></div>
            <div class="span6">
                <div style="border-radius: 5px; margin-top: 40px; background-color: #AAA; padding: 30px;">
                <form class="form-horizontal" action="j_security_check">
                    <div class="control-group">
                        <label for="usuario" class="control-label">Usuario</label>
                        <div class="controls">
                            <input  name="j_username" type="text" class="form-control" id="usuario"/>
                        </div>
                    </div>
                    <div class="control-group">
                        <label for="inputPassword1" class="control-label">Contraseña</label>
                        <div class="controls">
                            <input name="j_password" type="password" class="form-control" id="inputPassword1"/>
                        </div>
                    </div>           
                    <div class="control-group">
                        <div class="controls">
                            <button type="submit" class="btn btn-primary">Entrar</button>
                        </div>
                    </div>
                </form>
                    </div>
            </div>
            <div class="span3"></div>
        </boot:cuerpo>
    </body>
</html>
