
<%@page contentType="text/html" pageEncoding="UTF-8"%> 

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar Libro</title>
        <%@include file="../WEB-INF/jspf/includes.jspf" %>
    </head>    
    <body>
        <%@include file="../WEB-INF/jspf/header.jspf" %>
        <boot:cuerpo id="librosNuevo">
            <div class="span2"></div>
            <div class="span8">
                <h2>Ver Libro</h2>
                <boot:accionForm servlet="Libros" id="validar">
                    <boot:input nombre="titulo" desactivado="true" label="Título" id="titulo" clase="input-xlarge" valor="${libro.titulo}" />                    
                    <c:forEach var="autor" items="${autores}" varStatus="autorStatus">
                        <boot:input nombre="autoresGuardar" desactivado="true"  clase="input-xlarge" label="Autor ${autorStatus.count}" id="autor ${autorStatus.count}" valor="${autor.normalizado}" />                    
                    </c:forEach>    
                    <boot:input nombre="isbn" desactivado="true" clase="input-xlarge" label="ISBN" id="isbn" valor="${libro.isbn}"/>
                    <boot:input nombre="issn"  desactivado="true" clase="input-xlarge" label="ISSN" id="issn" valor="${libro.issn}" />
                    <boot:input nombre="lugar"  desactivado="true" clase="input-xlarge" label="Lugar de Edición" id="lugar" valor="${libro.lugar}" />
                    <boot:input nombre="editorial" desactivado="true" clase="input-xlarge" label="Editorial" id="editorial"valor="${libro.editorial}"  />
                    <boot:input nombre="anno" desactivado="true" clase="input-xlarge" label="Año de Edición" id="anno" valor="${libro.anno}" />
                    <boot:input nombre="materias"desactivado="true" clase="input-xlarge" label="Materias <br/> (separadas por comas)" id="autor" valor="${materiasString}" />                   
                </boot:accionForm>
                <boot:accionForm servlet="Libros">
                    <boot:accionButton tipo="submit"  etiqueta="Volver" valor="volverLibrosVer" />
                </boot:accionForm>
            </div>
            <div class="span2"></div>

        </boot:cuerpo>

    </body>

</html>
