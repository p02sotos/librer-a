<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Nuevo Libro</title>
        <%@include file="../WEB-INF/jspf/includes.jspf" %>
    </head>    
    <body>
        <%@include file="../WEB-INF/jspf/header.jspf" %>
        <boot:cuerpo id="librosNuevo">
            <div class="span2"></div>
            <div class="span8">
                <h2>Nuevo Libro</h2>
                <boot:accionForm servlet="Libros" id="validar">
                    <boot:input nombre="titulo" label="Título" id="titulo" valor="${libro.titulo}" />
                    <boot:accionButton tipo="submit" valor="agregarAutor" etiqueta="+ Agregar otro autor"  clase="btn-primary"/>
                    <c:forEach var="autor" items="${autores}" varStatus="autorStatus">
                        <boot:input nombre="autoresGuardar" label="Autor ${autorStatus.count}" id="autor ${autorStatus.count}" valor="${autor.normalizado}" />                    
                    </c:forEach>
                    <boot:input nombre="isbn" label="ISBN" id="isbn" valor="${libro.isbn}"/>
                    <boot:input nombre="issn" label="ISSN" id="issn" valor="${libro.issn}" />
                    <boot:input nombre="lugar" label="Lugar de Edición" id="lugar" valor="${libro.lugar}" />
                    <boot:input nombre="editorial" label="Editorial" id="editorial"valor="${libro.editorial}"  />
                    <boot:input nombre="anno" label="Año de Edición" id="anno" valor="${libro.anno}" />
                    <boot:input nombre="materias" label="Materias <br/> (separadas por comas)" id="autor" valor="${materiasString}" />
                    <boot:accionButton tipo="submit"  etiqueta="Seleccionar Estanteria" valor="seleccionarEstanteriaNueva" />
                    <c:out value="Estantería ${estanteria.signatura}"/>
                    <boot:accionButton tipo="submit" clase="btn-primary" etiqueta="Guardar" valor="guardarLibro" />
                </boot:accionForm>


                <boot:accionForm servlet="Libros">
                    <boot:accionButton tipo="submit"  etiqueta="Volver" valor="volverLibroNuevo" />
                </boot:accionForm>
            </div>
            <div class="span2"></div>
        </boot:cuerpo>
    </body>

</html>
