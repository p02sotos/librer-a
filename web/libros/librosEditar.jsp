
<%@page contentType="text/html" pageEncoding="UTF-8"%> 

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Editar Libro</title>
        <%@include file="../WEB-INF/jspf/includes.jspf" %>
    </head>    
    <body>
        <%@include file="../WEB-INF/jspf/header.jspf" %>
        <boot:cuerpo id="librosEditar">
            <div class="span2"></div>
            <div class="span8">
                <h2>Editar Libro</h2>
                <boot:accionForm servlet="Libros" id="validar">
                    <boot:input nombre="titulo" label="Título" id="titulo" clase="input-xlarge" valor="${libro.titulo}" />

                    <boot:accionButton tipo="submit" valor="agregarAutorModificar" etiqueta="+ Agregar otro autor"  clase="btn-primary"/>
                    <c:forEach var="autor" items="${autores}" varStatus="autorStatus">
                        <boot:input nombre="autoresGuardar"  clase="input-xlarge" label="Autor ${autorStatus.count}" id="autor ${autorStatus.count}" valor="${autor.normalizado}" />                    
                    </c:forEach>    
                    <boot:input nombre="isbn" clase="input-xlarge" label="ISBN" id="isbn" valor="${libro.isbn}"/>
                    <boot:input nombre="issn" clase="input-xlarge" label="ISSN" id="issn" valor="${libro.issn}" />
                    <boot:input nombre="lugar" clase="input-xlarge" label="Lugar de Edición" id="lugar" valor="${libro.lugar}" />
                    <boot:input nombre="editorial" clase="input-xlarge" label="Editorial" id="editorial"valor="${libro.editorial}"  />
                    <boot:input nombre="anno" clase="input-xlarge" label="Año de Edición" id="anno" valor="${libro.anno}" />
                    <boot:input nombre="materias" clase="input-xlarge" label="Materias <br/> (separadas por comas)" id="autor" valor="${materiasString}" />
                    <boot:accionButton tipo="submit"  etiqueta="Seleccionar Estanteria" valor="seleccionarEstanteriaEditar" />
                    <c:out value="Estantería ${libro.estanteria.signatura}"/>
                    <boot:accionButton tipo="submit" clase="btn-primary" etiqueta="Modificar" valor="libroModificar" />
                </boot:accionForm>
                <boot:accionForm servlet="Libros">
                    <boot:accionButton tipo="submit"  etiqueta="Volver" valor="volverLibroEditar" />
                </boot:accionForm>
            </div>
            <div class="span2"></div>

        </boot:cuerpo>

    </body>

</html>
