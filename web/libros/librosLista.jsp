<%-- 
    Document   : autoresLista
    Created on : 15-jul-2013, 22:04:57
    Author     : p02sotos
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>


<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Libros</title>
        <%@include file="../WEB-INF/jspf/includes.jspf" %>
        <script type="text/javascript">
            $(document).ready(function() {
                $('#TablaResultados').dataTable({
                    "oLanguage": {
                        "sUrl": "/Biblio/resources/traduccion.txt"
                    }
                });
            });
        </script>        
    </head>
    <body>
        <%@include file="../WEB-INF/jspf/header.jspf" %>
        <boot:cuerpo id="librosLIsta">
            <h1>Libros</h1>
            <boot:accionForm servlet="Libros">
                <boot:accionForm servlet="Libros">
                    <boot:accionButton tipo="submit" valor="libroNuevo" etiqueta="Nuevo Libro" clase="btn-primary" />
                </boot:accionForm>

                <table border="0" class="table table-condensed" id="TablaResultados">
                    <thead>
                        <tr>      
                            <th>Id</th>
                            <th>SIGNATURA</th>
                            <th>Autor/es</th>
                            <th>Título</th>
                            <th><span>ISBN</span></th>
                            <th><span>Materias</span></th>
                            <th><span>Lugar/Editor/Año</span></th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        <c:forEach var="actual" items="${libros}" varStatus="actualStatus">
                            <tr>
                                <td><c:out value="${actual.id}"/></td>
                                 <td><c:out value="${actual.estanteria.signatura}"/></td>
                                <td>
                                    <c:forEach var="autor" items="${actual.autores}">
                                        ${autor.normalizado}, 
                                    </c:forEach>                                    
                                </td>
                                <td><c:out value="${actual.titulo}"/></td>
                                <td><c:out value="${actual.isbn}"/></td>
                                <td>

                                    <c:out value="${actual.materiasString}"/>
                                </td> 
                                <td><c:out value="${actual.lugar} : ${actual.editorial},${actual.anno}"/></td>

                                <td class="botones-crud">  <form class="btn-group pull-left" id ="formulario" action="/Biblio/Libros" method="GET"> 
                                        <input class="btn btn-small " type="hidden" name="seleccion" value="${actualStatus.count}"/>
                                        <button class="btn btn-small" type="submit" value="libroVer" name="accion" title="Ver" />
                                        <span class="add-on"><i class="icon-eye-open"></i></span> 
                                        </button>
                                        <button class="btn btn-small btn-info" type="submit" value="libroEditar" name="accion" title="Editar" />
                                        <span class="add-on"><i class="icon-pencil"></i></span> 
                                        </button>
                                        <button class="btn btn-danger btn-small" type="submit" value="Borrar" name="accion" title="Borrar" />
                                        <span class="add-on"><i class="icon-trash"></i></span> 
                                        </button>
                                    </form>
                                </td>
                            </c:forEach>

                    </tbody>
                </table>


                <c:if test="${requestScope.confirmar}">
                    <boot:accionForm servlet="Libros">
                        <div id ="mensajeBorrado">
                            <div class="alert alert-block">
                                <p class="text-center">
                                    ¿Seguro que desea borrar?
                                </p>
                                <p class="text-center">
                                    <input class="btn" type="submit" value="Confirmar" name="accion" />
                                    <input class="btn" type="submit" value="Cancelar" name="accion" />
                                </p>
                            </div>
                        </div>
                    </boot:accionForm>
                </c:if>

            </boot:accionForm>
        </boot:cuerpo>


    </body>

</html>
